// Fill out your copyright notice in the Description page of Project Settings.


#include "MagazinDrop.h"
#include "Types.h"

// Sets default values
AMagazinDrop::AMagazinDrop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MagazinMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Magazin Drop Mesh"));
	MagazinMesh->SetupAttachment(RootComponent);
	MagazinMesh->SetCanEverAffectNavigation(false);

	

}

// Called when the game starts or when spawned
void AMagazinDrop::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMagazinDrop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMagazinDrop::InitMagazin(FWeaponInfo InitParam)
{
	MagazinMesh->SetStaticMesh(InitParam.MagazineDrop);
}

