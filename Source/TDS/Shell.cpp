// Fill out your copyright notice in the Description page of Project Settings.


#include "Shell.h"
#include "Types.h"

// Sets default values
AShell::AShell()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ShellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	ShellMesh->SetupAttachment(RootComponent);
	ShellMesh->SetCanEverAffectNavigation(false);

}

// Called when the game starts or when spawned
void AShell::BeginPlay()
{

	Super::BeginPlay();

}

// Called every frame
void AShell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AShell::InitShell(FProjectileInfo InitParam)
{
	ShellMesh->SetStaticMesh(InitParam.ShellBullets);
}
