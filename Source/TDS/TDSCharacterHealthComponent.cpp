// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterHealthComponent.h"



void UTDSCharacterHealthComponent::ChangeCharacterHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield <= 0.0f)
		{

			OnShieldBroken.Broadcast();
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTDSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{

	if (!IsShield && ChangeValue > 0)
	{
		OnShieldRepared.Broadcast();
		IsShield = true;
	}

	if (IsShield && Shield + ChangeValue <= 0 && Shield > 0)
	{
		OnShieldBroken.Broadcast();
		IsShield = false;
	}

	if (ChangeValue < 0)
	{
		IsRecoveringShieldEnable = false;

		if (GetWorld() && !IsRecoveringShield)
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}

	Shield += ChangeValue;

	OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;

	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

}

void UTDSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);

		IsRecoveringShieldEnable = true;
	}
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
	if(IsRecoveringShieldEnable)
	{
		IsRecoveringShield = true;

		float tmp = Shield;
		tmp = tmp + ShieldRecoverValue;
		if (tmp > 100.0f)
		{
			Shield = 100.0f;
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
				IsRecoveringShield = false;
			}
		}
		else
		{
			//Shield = tmp;

			ChangeShieldValue(ShieldRecoverValue);
		}
	}
	else
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			IsRecoveringShield = false;
		}
	}

	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}
